# example-ml-packaging-pipeline

In [this project](https://www.freecodecamp.org/news/ml-model-publishing-with-gitlab-package-registry/) you will find an example CI/CD pipeline for packaging and distribution of a trained model in object recognition.
The goal is to show a simple way of using CI/CD to make your model available as a command line interface automatically.

## Docker Building
```bash
docker build -t pipeline:latest .
docker images
docker run -idt --name pipeline <IMAGE ID>
docker attach pipeline
```
